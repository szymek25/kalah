package com.szymanski.Kalah.services;

import com.szymanski.Kalah.entities.Game;
import com.szymanski.Kalah.enums.Player;

import java.util.Map;

public interface GameEngine {

    /**
     * Will make operations at game board
     *
     * @param pit pitNumber
     * @param game game
     * @return if true then next player move
     */
    boolean makeMove(int pit, Game game);

    /**
     * Will move stones from each player`s pit into its Kalah
     *
     * @param player player
     * @param game game
     */
    Game moveEachStoneIntoKalahForPlayer(Player player, Game game);

    /**
     * returns winner of game or null if draw
     *
     * @param game game
     * @return player or null if draw
     */
    Player getGameWinner(Game game);

    /**
     * checks if player have empty pits
     * @param player player enum
     * @param board board
     * @return boolean
     */
    boolean checkIfPlayerHaveEmptyPits(Player player, Map<Integer, Integer> board);
}
