package com.szymanski.Kalah.services;

import com.szymanski.Kalah.dtos.NewGameDTO;
import com.szymanski.Kalah.entities.Game;
import com.szymanski.Kalah.enums.Player;
import com.szymanski.Kalah.exceptions.WrongPlayerMoveException;

public interface GameService {

    /**
     * Will create and initialize new game
     *
     * @return new game
     */
    Game createGame();

    /**
     *
     * @param pit should be number from range 1-6
     * @param gameId gameId
     * @param player player move
     * @return current game state
     * @throws WrongPlayerMoveException will be thrown if wrong player will attempt make move
     * @throws IllegalStateException will be thrown if game doesn`t exists
     */
    Game makeMove(int pit, long gameId, Player player) throws WrongPlayerMoveException, IllegalStateException;
}
