package com.szymanski.Kalah.services.impl;

import com.szymanski.Kalah.entities.Game;
import com.szymanski.Kalah.enums.GameStatus;
import com.szymanski.Kalah.enums.Player;
import com.szymanski.Kalah.exceptions.WrongPlayerMoveException;
import com.szymanski.Kalah.repositories.GameRepository;
import com.szymanski.Kalah.services.GameEngine;
import com.szymanski.Kalah.services.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class GameServiceImpl implements GameService {


    public static final int INITIAL_VALUE_OF_STONES = 6;


    @Autowired
    protected GameRepository gameRepository;

    @Autowired
    protected GameEngine gameEngine;

    @Override
    public Game createGame() {
        final Map<Integer, Integer> board = createInitialBoard();
        return gameRepository.save(new Game(board));
    }

    @Override
    public Game makeMove(int pit, final long gameId, final Player player) throws WrongPlayerMoveException, IllegalStateException {
        Game game = gameRepository.findByGameId(gameId);
        if (game == null) {
            throw new IllegalStateException();
        }
        final Player moveOfPlayer = game.getMoveOfPlayer();
        if (!moveOfPlayer.equals(player)) {
            throw new WrongPlayerMoveException();
        }
        if (Player.SECOND.equals(player)) {
            pit = pit + 7;
        }
        boolean nextPlayerMove = gameEngine.makeMove(pit, game);

        final Player playerWithEmptyPits = checkIfSomePlayerHaveEmptyPits(game);
        if (playerWithEmptyPits != null) {
            moveOpponentStones(playerWithEmptyPits, game);
            final Player gameWinner = gameEngine.getGameWinner(game);
            game.setMoveOfPlayer(gameWinner);
            game.setGameStatus(GameStatus.ENDED);
        } else if (nextPlayerMove) {
            setNextPlayerMove(game, moveOfPlayer);
        }

        gameRepository.save(game);
        return game;
    }

    private void moveOpponentStones(Player playerWithEmptyPits, Game game) {
        switch (playerWithEmptyPits) {
            case FIRST:
                gameEngine.moveEachStoneIntoKalahForPlayer(Player.SECOND, game);
                break;
            case SECOND:
                gameEngine.moveEachStoneIntoKalahForPlayer(Player.FIRST, game);

        }
    }

    private Player checkIfSomePlayerHaveEmptyPits(final Game game) {
        final Map<Integer, Integer> board = game.getBoard();
        final boolean firstPlayerHasEmptyPits = gameEngine.checkIfPlayerHaveEmptyPits(Player.FIRST, board);
        final boolean secondPlayerHasEmptyPits = gameEngine.checkIfPlayerHaveEmptyPits(Player.SECOND, board);


        return (!firstPlayerHasEmptyPits && !secondPlayerHasEmptyPits) ? null : (firstPlayerHasEmptyPits ? Player.FIRST : Player.SECOND);
    }



    private void setNextPlayerMove(final Game game, final Player moveOfPlayer) {
        if (Player.FIRST.equals(moveOfPlayer)) {
            game.setMoveOfPlayer(Player.SECOND);
        } else {
            game.setMoveOfPlayer(Player.FIRST);
        }
    }


    protected Map<Integer, Integer> createInitialBoard() {
        final Map<Integer, Integer> board = new HashMap<>();
        board.put(1, INITIAL_VALUE_OF_STONES);
        board.put(2, INITIAL_VALUE_OF_STONES);
        board.put(3, INITIAL_VALUE_OF_STONES);
        board.put(4, INITIAL_VALUE_OF_STONES);
        board.put(5, INITIAL_VALUE_OF_STONES);
        board.put(6, INITIAL_VALUE_OF_STONES);
        board.put(7, 0);
        board.put(8, INITIAL_VALUE_OF_STONES);
        board.put(9, INITIAL_VALUE_OF_STONES);
        board.put(10, INITIAL_VALUE_OF_STONES);
        board.put(11, INITIAL_VALUE_OF_STONES);
        board.put(12, INITIAL_VALUE_OF_STONES);
        board.put(13, INITIAL_VALUE_OF_STONES);
        board.put(14, 0);
        return board;
    }
}
