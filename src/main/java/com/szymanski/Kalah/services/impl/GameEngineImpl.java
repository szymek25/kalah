package com.szymanski.Kalah.services.impl;

import com.szymanski.Kalah.entities.Game;
import com.szymanski.Kalah.enums.Player;
import com.szymanski.Kalah.services.GameEngine;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class GameEngineImpl implements GameEngine {

    private static final int FIRST_PLAYER_KALAH = 7;
    private static final int SECOND_PLAYER_KALAH = 14;

    @Override
    public boolean makeMove(final int pit, final Game game) {
        final Map<Integer, Integer> board = game.getBoard();
        final Player moveOfPlayer = game.getMoveOfPlayer();
        final Integer movesToBeDone = board.get(pit);
        board.put(pit, 0);
        Integer lastVisitedPit = pit;
        for (int i = 0; i < movesToBeDone; i++) {
            int nextPit = lastVisitedPit + 1;
            if (isOpponentKalah(nextPit, moveOfPlayer)) {
                nextPit += 1;
            }
            if (nextPit > 14) {
                nextPit = 1;
            }
            Integer currentStateOfPit = board.get(nextPit);
            board.put(nextPit, currentStateOfPit + 1);
            lastVisitedPit = nextPit;
        }

        if (board.get(lastVisitedPit) == 1 && pitBelongsToPlayer(lastVisitedPit, moveOfPlayer)) {
            getStonesFromCurrentAndOpponetsPit(lastVisitedPit, board, moveOfPlayer);
        }

        game.setBoard(board);

        return !isCurrentPlayerKalah(lastVisitedPit, moveOfPlayer);
    }

    private boolean pitBelongsToPlayer(final Integer pit, final Player moveOfPlayer) {
        if (Player.FIRST.equals(moveOfPlayer)) {
            return pit < 7;
        } else {
            return pit > 7 && pit < 14;
        }
    }

    @Override
    public Game moveEachStoneIntoKalahForPlayer(final Player player, final Game game) {
        int pit;
        if (Player.FIRST.equals(player)) {
            pit = 1;
        } else {
            pit = 8;
        }
        final Map<Integer, Integer> board = game.getBoard();
        int stones = 0;
        for (int i = 0; i <= 6; i++) {
            stones += board.get(pit);
            board.put(pit, 0);
            pit += 1;
        }
        Integer currentKalahState;
        if (Player.FIRST.equals(player)) {
            currentKalahState = board.get(FIRST_PLAYER_KALAH);
            board.put(FIRST_PLAYER_KALAH, currentKalahState + stones);
        } else {
            currentKalahState = board.get(SECOND_PLAYER_KALAH);
            board.put(SECOND_PLAYER_KALAH, currentKalahState + stones);
        }
        game.setBoard(board);

        return game;
    }

    @Override
    public Player getGameWinner(final Game game) {
        final Map<Integer, Integer> board = game.getBoard();
        final int firstPlayerScore = board.get(FIRST_PLAYER_KALAH);
        final int secondPlayerScore = board.get(SECOND_PLAYER_KALAH);
        if (firstPlayerScore == secondPlayerScore) {
            return null;
        } else if (firstPlayerScore > secondPlayerScore) {
            return Player.FIRST;
        } else {
            return Player.SECOND;
        }
    }

    @Override
    public boolean checkIfPlayerHaveEmptyPits(Player player, Map<Integer, Integer> board) {
        boolean isAnyPitNotEmpty = false;
        int pit;
        if (Player.FIRST.equals(player)) {
            pit = 1;
        } else {
            pit = 8;
        }
        for (int i = 0; i < 6; i++) {
            if (board.get(pit) > 0) {
                isAnyPitNotEmpty = true;
                break;
            }
            pit += 1;
        }

        return !isAnyPitNotEmpty;
    }

    private void getStonesFromCurrentAndOpponetsPit(final Integer pit, final Map<Integer, Integer> board, final Player moveOfPlayer) {
        final Integer currentPitValue = board.get(pit);
        board.put(pit, 0);
        final Integer opponentsPit;
        if (Player.FIRST.equals(moveOfPlayer)) {
            opponentsPit = pit + 7;
        } else {
            opponentsPit = pit - 7;
        }
        final Integer opponentsPitValue = board.get(opponentsPit);
        board.put(opponentsPit, 0);
        final Integer currentKalahValue;
        if (Player.FIRST.equals(moveOfPlayer)) {
            currentKalahValue = board.get(FIRST_PLAYER_KALAH);
            board.put(FIRST_PLAYER_KALAH, currentKalahValue + currentPitValue + opponentsPitValue);
        } else {
            currentKalahValue = board.get(SECOND_PLAYER_KALAH);
            board.put(SECOND_PLAYER_KALAH, currentKalahValue + currentPitValue + opponentsPitValue);
        }
    }

    protected boolean isCurrentPlayerKalah(final int pit, final Player player) {
        switch (player) {
            case FIRST:
                return FIRST_PLAYER_KALAH == pit;
            case SECOND:
                return SECOND_PLAYER_KALAH == pit;
            default:
                return false;
        }
    }

    protected boolean isOpponentKalah(final int pit, final Player player) {
        switch (player) {
            case FIRST:
                return SECOND_PLAYER_KALAH == pit;
            case SECOND:
                return FIRST_PLAYER_KALAH == pit;
            default:
                return false;
        }
    }
}
