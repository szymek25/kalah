package com.szymanski.Kalah.controllers;

import com.szymanski.Kalah.dtos.GameStatusDTO;
import com.szymanski.Kalah.dtos.NewGameDTO;
import com.szymanski.Kalah.facade.GameFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/games")
public class GamesController {

    @Autowired
    protected GameFacade gameFacade;

    @PostMapping
    public NewGameDTO createGame() {
        NewGameDTO newGame = gameFacade.createNewGame();

        return newGame;
    }

    @PutMapping("/{gameId}")
    public GameStatusDTO makeMove(@PathVariable("gameId") final Long gameId, @RequestParam("player") final Integer player, @RequestParam("pit") final Integer pit) {
        return gameFacade.makeMove(pit, gameId, player);
    }
}
