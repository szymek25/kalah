package com.szymanski.Kalah.enums;

public enum Player {
    FIRST(1), SECOND(2);

    private int id;

    Player(final int id) {
        this.id = id;
    }
}
