package com.szymanski.Kalah.dtos;

public class NewGameDTO {

    private Long gameId;
    private String gameURL;

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(final Long gameId) {
        this.gameId = gameId;
    }

    public String getGameURL() {
        return gameURL;
    }

    public void setGameURL(final String gameURL) {
        this.gameURL = gameURL;
    }
}
