package com.szymanski.Kalah.dtos;

import java.util.Map;

public class GameStatusDTO extends NewGameDTO {

    private Map<String, String> status;

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public Map<String, String> getStatus() {
        return status;
    }

    public void setStatus(final Map<String, String> status) {
        this.status = status;
    }
}
