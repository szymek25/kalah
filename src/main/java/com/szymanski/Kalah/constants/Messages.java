package com.szymanski.Kalah.constants;

/**
 * Should be replaced with localization
 */
public class Messages {

    public static final String WRONG_PLAYER_MOVE = "Now it is your opponent`s move";
    public static final String FIRST_PLAYER_MOVE = "First player";
    public static final String SECOND_PLAYER_MOVE = "Second player";
    public static final String WRONG_PLAYER_NUMBER = "Please input proper number of player";
    public static final String WRONG_PIT_NUMBER = "Please input proper number of pit";
    public static final String FIRST_PLAYER_WON = "First player is the winner";
    public static final String SECOND_PLAYER_WON = "Second player is the winner";
    public static final String GAME_DOES_NOT_EXIST = "Game with provided id doesn`t exist";
    public static final String GAME_ENDED_WITH_DRAW = "Game ended with draw";

}
