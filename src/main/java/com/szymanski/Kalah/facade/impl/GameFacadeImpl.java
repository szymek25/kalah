package com.szymanski.Kalah.facade.impl;

import com.szymanski.Kalah.constants.Messages;
import com.szymanski.Kalah.dtos.GameStatusDTO;
import com.szymanski.Kalah.dtos.NewGameDTO;
import com.szymanski.Kalah.entities.Game;
import com.szymanski.Kalah.enums.GameStatus;
import com.szymanski.Kalah.enums.Player;
import com.szymanski.Kalah.exceptions.WrongPlayerMoveException;
import com.szymanski.Kalah.facade.GameFacade;
import com.szymanski.Kalah.services.GameService;
import org.apache.commons.text.StringSubstitutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class GameFacadeImpl implements GameFacade {

    private static final String LOCAL_SERVER_PORT = "local.server.port";
    private static final String APPLICATION_HOST = "application.host";
    private static final String HOST_NAME = "hostName";
    private static final String PORT = "port";
    private static final String GAME_ID = "gameId";
    private static final String URL_PATTERN = "http://${hostName}:${port}/api/v1/games/${gameId}";

    @Autowired
    private GameService gameService;

    @Autowired
    protected Environment environment;


    @Override
    public NewGameDTO createNewGame() {
        Game newGame = gameService.createGame();
        final NewGameDTO newGameDTO = new NewGameDTO();
        newGameDTO.setGameId(newGame.getGameId());
        newGameDTO.setGameURL(getGameUrl(newGameDTO.getGameId()));


        return newGameDTO;
    }

    @Override
    public GameStatusDTO makeMove(final int pit, final long gameId, final int playerNumber) {
        GameStatusDTO gameStatusDTO = new GameStatusDTO();
        if (pit > 6) {
            gameStatusDTO.setMessage(Messages.WRONG_PIT_NUMBER);
            return gameStatusDTO;
        }
        Player player = getPlayerEnumValue(playerNumber);
        if (player == null) {
            gameStatusDTO.setMessage(Messages.WRONG_PLAYER_NUMBER);
        }

        try {
            final Game game = gameService.makeMove(pit, gameId, player);
            final Map<String, String> status = convertGameBoard(game.getBoard());
            gameStatusDTO.setStatus(status);
            if (GameStatus.ENDED.equals(game.getGameStatus())) {
                setMessageForWinner(game, gameStatusDTO);
            } else {
                setNextPlayerMoveMessage(game, gameStatusDTO);
            }
            gameStatusDTO.setGameId(gameId);
            gameStatusDTO.setGameURL(getGameUrl(gameId));
        } catch (WrongPlayerMoveException e) {
            gameStatusDTO.setMessage(Messages.WRONG_PLAYER_MOVE);
        } catch (IllegalStateException e) {
            gameStatusDTO.setMessage(Messages.GAME_DOES_NOT_EXIST);
        } finally {
            return gameStatusDTO;
        }
    }

    protected void setNextPlayerMoveMessage(final Game game, final GameStatusDTO gameStatusDTO) {
        Player moveOfPlayer = game.getMoveOfPlayer();
        switch (moveOfPlayer) {
            case FIRST:
                gameStatusDTO.setMessage(Messages.FIRST_PLAYER_MOVE);
                break;
            case SECOND:
                gameStatusDTO.setMessage(Messages.SECOND_PLAYER_MOVE);
        }
    }

    protected void setMessageForWinner(final Game game, final GameStatusDTO gameStatusDTO) {
        final Player moveOfPlayer = game.getMoveOfPlayer();
        if (moveOfPlayer != null) {
            switch (moveOfPlayer) {
                case FIRST:
                    gameStatusDTO.setMessage(Messages.FIRST_PLAYER_WON);
                    break;
                case SECOND:
                    gameStatusDTO.setMessage(Messages.SECOND_PLAYER_WON);
                    break;
            }
        } else {
            gameStatusDTO.setMessage(Messages.GAME_ENDED_WITH_DRAW);
        }
    }

    protected Map<String, String> convertGameBoard(final Map<Integer, Integer> board) {
        LinkedHashMap<String, String> sortedMap = new LinkedHashMap<>();
        board.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .forEachOrdered(x -> sortedMap.put(String.valueOf(x.getKey()), String.valueOf(x.getValue())));

        return sortedMap;
    }

    protected Player getPlayerEnumValue(final int playerNumber) {
        switch (playerNumber) {
            case 1:
                return Player.FIRST;
            case 2:
                return Player.SECOND;
            default:
                return null;
        }
    }

    protected String getGameUrl(final long gameId) {
        final String port = environment.getProperty(LOCAL_SERVER_PORT);
        final String hostName = environment.getProperty(APPLICATION_HOST);
        Map<String, String> valuesMap = new HashMap<>();
        valuesMap.put(HOST_NAME, hostName);
        valuesMap.put(PORT, port);
        valuesMap.put(GAME_ID, String.valueOf(gameId));
        final StringSubstitutor sub = new StringSubstitutor(valuesMap);

        return sub.replace(URL_PATTERN);
    }
}
