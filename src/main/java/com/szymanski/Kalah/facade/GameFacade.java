package com.szymanski.Kalah.facade;

import com.szymanski.Kalah.dtos.GameStatusDTO;
import com.szymanski.Kalah.dtos.NewGameDTO;
import com.szymanski.Kalah.enums.Player;

public interface GameFacade {

    /**
     * Will create and initialize new game
     *
     * @return new game data transfer object
     */
    NewGameDTO createNewGame();

    /**
     * Will make move and update game state. In response current game status
     *
     * @param pit should be number from 1 to 6
     * @param gameId gameID
     * @param playerNumber player number from 1 to 2
     * @return game status
     */
    GameStatusDTO makeMove(int pit, long gameId, int playerNumber);
}
