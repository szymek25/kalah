package com.szymanski.Kalah.repositories;

import com.szymanski.Kalah.entities.Game;
import org.springframework.data.repository.CrudRepository;

public interface GameRepository extends CrudRepository<Game, Long> {

    Game findByGameId(long gameId);
}
