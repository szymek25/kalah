package com.szymanski.Kalah.entities;

import com.szymanski.Kalah.enums.GameStatus;
import com.szymanski.Kalah.enums.Player;

import javax.persistence.*;
import java.util.Map;

@Entity
public class Game {

    public Game() {
        //Empty constructor
    }

    public Game(final Map<Integer, Integer> board) {
        this.board = board;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long gameId;

    @Column
    private Player moveOfPlayer = Player.FIRST;

    @ElementCollection
    @MapKeyColumn(name = "potContent")
    @Column(name = "potNumber")
    @CollectionTable(name = "pot_types", joinColumns = @JoinColumn(name = "pot_id"))
    private Map<Integer, Integer> board;

    @Column
    private GameStatus gameStatus = GameStatus.IN_PROGRESS;


    public long getGameId() {
        return gameId;
    }

    public void setGameId(final long gameId) {
        this.gameId = gameId;
    }

    public Player getMoveOfPlayer() {
        return moveOfPlayer;
    }

    public void setMoveOfPlayer(final Player moveOfPlayer) {
        this.moveOfPlayer = moveOfPlayer;
    }

    public Map<Integer, Integer> getBoard() {
        return board;
    }

    public void setBoard(final Map<Integer, Integer> board) {
        this.board = board;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(final GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }
}
