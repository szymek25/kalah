package com.szymanski.Kalah.services.impl;

import com.szymanski.Kalah.entities.Game;
import com.szymanski.Kalah.enums.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GameEngineImplTest {

    private GameEngineImpl gameEngine;

    private GameServiceImpl gameService;

    @BeforeEach
    void setUp() {
        this.gameEngine = new GameEngineImpl();
        this.gameService = new GameServiceImpl();
    }

    @Test
    public void shouldMoveStonesForFirstPlayer() {
        final Game game = new Game(gameService.createInitialBoard());
        game.setMoveOfPlayer(Player.FIRST);
        final boolean nextPlayerMove = gameEngine.makeMove(2, game);
        final Map<Integer, Integer> board = game.getBoard();
        assertEquals(0, board.get(2));
        assertEquals(7, board.get(3));
        assertEquals(7, board.get(4));
        assertEquals(7, board.get(5));
        assertEquals(7, board.get(6));
        assertEquals(1, board.get(7));
        assertEquals(7, board.get(8));
        assertEquals(true, nextPlayerMove);
    }

    @Test
    public void shouldMoveStonesForSecondPlayer() {
        final Game game = new Game(gameService.createInitialBoard());
        game.setMoveOfPlayer(Player.SECOND);
        boolean nextPlayerMove = gameEngine.makeMove(9, game);
        final Map<Integer, Integer> board = game.getBoard();
        assertEquals(0, board.get(9));
        assertEquals(7, board.get(10));
        assertEquals(7, board.get(11));
        assertEquals(7, board.get(12));
        assertEquals(7, board.get(13));
        assertEquals(1, board.get(14));
        assertEquals(7, board.get(1));
        assertEquals(true, nextPlayerMove);
    }

    @Test
    public void shouldSkipKalahOfSecondPlayer() {
        final Game game = new Game(createStubBoard(0, 0, 5, 0, 1, 10, 2, 0, 2, 3, 1, 3, 10, 0));
        game.setMoveOfPlayer(Player.FIRST);
        gameEngine.makeMove(6, game);
        final Map<Integer, Integer> board = game.getBoard();
        assertEquals(3, board.get(7));
        assertEquals(0, board.get(14));
        assertEquals(1, board.get(1));
    }


    @Test
    public void shouldSkipKalahOfFirstPlayer() {
        final Game game = new Game(createStubBoard(0, 0, 5, 0, 1, 10, 2, 0, 2, 3, 1, 3, 10, 0));
        game.setMoveOfPlayer(Player.SECOND);
        gameEngine.makeMove(13, game);
        final Map<Integer, Integer> board = game.getBoard();
        assertEquals(1, board.get(14));
        assertEquals(2, board.get(7));
        assertEquals(1, board.get(8));

    }

    @Test
    public void shouldGetStonesFromOpponentPot() {
        final Game game1 = new Game(createStubBoard(0, 0, 5, 0, 1, 8, 2, 3, 0, 0, 1, 3, 10, 0));
        game1.setMoveOfPlayer(Player.FIRST);
        gameEngine.makeMove(6, game1);
        final Map<Integer, Integer> board1 = game1.getBoard();
        assertEquals(0, board1.get(1));
        assertEquals(8, board1.get(7));
        assertEquals(0, board1.get(8));


        final Game game2 = new Game(createStubBoard(0, 0, 5, 0, 1, 8, 2, 3, 0, 0, 1, 3, 10, 0));
        game2.setMoveOfPlayer(Player.SECOND);
        gameEngine.makeMove(13, game2);
        final Map<Integer, Integer> board2 = game2.getBoard();
        assertEquals(0, board2.get(10));
        assertEquals(8, board2.get(14));
        assertEquals(0, board2.get(3));

    }

    @Test
    public void firstPlayerShouldGetNextMove() {
        final Game game = new Game(gameService.createInitialBoard());
        game.setMoveOfPlayer(Player.FIRST);
        final boolean nextPlayerMove = gameEngine.makeMove(1, game);

        assertEquals(false, nextPlayerMove);
    }

    @Test
    public void secondPlayerShouldGetNextMove() {
        final Game game = new Game(gameService.createInitialBoard());
        game.setMoveOfPlayer(Player.SECOND);
        final boolean nextPlayerMove = gameEngine.makeMove(8, game);

        assertEquals(false, nextPlayerMove);
    }

    @Test
    public void shouldMoveStonesIntoKalah() {
        final Map<Integer, Integer> stubBoard1 = createStubBoard(2, 2, 5, 1, 2, 2, 1, 3, 0, 0, 1, 3, 10, 0);
        final Game game1 = new Game(stubBoard1);
        gameEngine.moveEachStoneIntoKalahForPlayer(Player.FIRST, game1);
        final Map<Integer, Integer> board1 = game1.getBoard();
        assertEquals(0, board1.get(1));
        assertEquals(0, board1.get(2));
        assertEquals(0, board1.get(3));
        assertEquals(0, board1.get(4));
        assertEquals(0, board1.get(5));
        assertEquals(0, board1.get(6));
        assertEquals(15, board1.get(7));
        assertEquals(3, board1.get(8));
        assertEquals(0, board1.get(9));
        assertEquals(0, board1.get(10));
        assertEquals(1, board1.get(11));
        assertEquals(3, board1.get(12));
        assertEquals(10, board1.get(13));
        assertEquals(0, board1.get(14));

        final Map<Integer, Integer> stubBoard2 = createStubBoard(2, 2, 5, 1, 2, 2, 1, 3, 0, 0, 1, 3, 10, 0);
        final Game game2 = new Game(stubBoard2);
        gameEngine.moveEachStoneIntoKalahForPlayer(Player.SECOND, game2);
        final Map<Integer, Integer> board2 = game2.getBoard();
        assertEquals(2, board2.get(1));
        assertEquals(2, board2.get(2));
        assertEquals(5, board2.get(3));
        assertEquals(1, board2.get(4));
        assertEquals(2, board2.get(5));
        assertEquals(2, board2.get(6));
        assertEquals(1, board2.get(7));
        assertEquals(0, board2.get(8));
        assertEquals(0, board2.get(9));
        assertEquals(0, board2.get(10));
        assertEquals(0, board2.get(11));
        assertEquals(0, board2.get(12));
        assertEquals(0, board2.get(13));
        assertEquals(17, board2.get(14));
    }

    @Test
    public void shouldReturnWinnerOfGame() {
        final Player gameWinner1 = gameEngine.getGameWinner(new Game(createStubBoard(0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 8)));
        assertEquals(Player.FIRST, gameWinner1);

        final Player gameWinner2 = gameEngine.getGameWinner(new Game(createStubBoard(0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 11)));
        assertEquals(Player.SECOND, gameWinner2);

        final Player gameWinner3 = gameEngine.getGameWinner(new Game(createStubBoard(0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 10)));
        Assertions.assertNull(gameWinner3);
    }

    @Test
    public void checkIfPlayerHaveEmptyPits() {
        final boolean playerOneHaveEmptyPits = gameEngine.checkIfPlayerHaveEmptyPits(Player.FIRST, createStubBoard(0, 0, 0, 0, 0, 0, 10, 0, 0, 3, 0, 0, 0, 8));
        assertTrue(playerOneHaveEmptyPits);

        final boolean playerTwoHaveEmptyPits = gameEngine.checkIfPlayerHaveEmptyPits(Player.SECOND, createStubBoard(0, 0, 3 ,2, 0, 0, 10, 0, 0, 0, 0, 0, 0, 8));
        assertTrue(playerTwoHaveEmptyPits);

        final boolean playerTwoDoesNotHaveEmptyPits = gameEngine.checkIfPlayerHaveEmptyPits(Player.SECOND, createStubBoard(0, 0, 0, 0, 0, 0, 10, 0, 0, 3, 0, 0, 0, 8));

        assertFalse(playerTwoDoesNotHaveEmptyPits);
    }

    protected Map<Integer, Integer> createStubBoard(final Integer... values) {
        final Map<Integer, Integer> board = new HashMap<>();
        for (int i = 1; i <= values.length; i++) {
            board.put(i, values[i - 1]);
        }
        return board;
    }
}