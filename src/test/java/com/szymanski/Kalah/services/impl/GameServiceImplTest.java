package com.szymanski.Kalah.services.impl;

import com.szymanski.Kalah.entities.Game;
import com.szymanski.Kalah.enums.GameStatus;
import com.szymanski.Kalah.enums.Player;
import com.szymanski.Kalah.exceptions.WrongPlayerMoveException;
import com.szymanski.Kalah.repositories.GameRepository;
import com.szymanski.Kalah.services.GameEngine;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
class GameServiceImplTest {


    @Mock
    private GameRepository gameRepository;

    @Mock
    private GameEngine gameEngine;

    @InjectMocks
    private GameServiceImpl gameService;

    @Test
    void firstPlayerMakeMove() throws WrongPlayerMoveException {
        when(gameRepository.findByGameId(anyLong())).thenReturn(getStubGame(Player.FIRST, gameService.createInitialBoard()));
        when(gameEngine.makeMove(anyInt(), any())).thenReturn(true);
        Game game = gameService.makeMove(2, 1, Player.FIRST);

        assertEquals(Player.SECOND, game.getMoveOfPlayer());
        assertEquals(GameStatus.IN_PROGRESS, game.getGameStatus());
        verify(gameEngine).makeMove(eq(2), any());
    }

    @Test
    void secondPlayerShouldHaveNextMove() throws WrongPlayerMoveException {
        when(gameRepository.findByGameId(anyLong())).thenReturn(getStubGame(Player.SECOND, gameService.createInitialBoard()));
        when(gameEngine.makeMove(anyInt(), any())).thenReturn(false);
        Game game = gameService.makeMove(3, 1, Player.SECOND);

        assertEquals(Player.SECOND, game.getMoveOfPlayer());
        verify(gameEngine).makeMove(eq(10), any());
    }

    @Test
    void wrongPlayerMoveExceptionShouldBeThrown() {
        when(gameRepository.findByGameId(anyLong())).thenReturn(getStubGame(Player.SECOND, gameService.createInitialBoard()));

        assertThrows(WrongPlayerMoveException.class, () -> {
            gameService.makeMove(1, 1, Player.FIRST);
        });
    }

    @Test
    void shouldThrowIllegalStateExceptionIfGameDoesNotExist() {
        assertThrows(IllegalStateException.class, () -> {
            gameService.makeMove(1, 1, Player.FIRST);
        });
    }

    @Test
    void firstPlayerShouldBeWinner() throws WrongPlayerMoveException {
        when(gameRepository.findByGameId(anyLong())).thenReturn(getStubGame(Player.FIRST, createStubBoard(0, 0, 0, 0, 0, 3, 15, 0, 0, 0, 0, 0, 0, 4)));
        when(gameEngine.moveEachStoneIntoKalahForPlayer(any(),any())).thenReturn(getStubGame(Player.FIRST, createStubBoard(0, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 0, 6)));
        when(gameEngine.checkIfPlayerHaveEmptyPits(eq(Player.FIRST),any())).thenReturn(true);
        when(gameEngine.checkIfPlayerHaveEmptyPits(eq(Player.SECOND),any())).thenReturn(false);
        when(gameEngine.getGameWinner(any())).thenReturn(Player.FIRST);

        Game game1 = gameService.makeMove(6, 2, Player.FIRST);

        assertEquals(GameStatus.ENDED, game1.getGameStatus());
        assertEquals(Player.FIRST, game1.getMoveOfPlayer());

        when(gameRepository.findByGameId(anyLong())).thenReturn(getStubGame(Player.SECOND, createStubBoard(0, 0, 0, 0, 0, 3, 15, 0, 0, 0, 0, 0, 3, 4)));
        when(gameEngine.moveEachStoneIntoKalahForPlayer(any(),any())).thenReturn(getStubGame(Player.SECOND, createStubBoard(0, 0, 0, 0, 0, 3, 20, 0, 0, 0, 0, 0, 0, 5)));
        when(gameEngine.checkIfPlayerHaveEmptyPits(eq(Player.FIRST),any())).thenReturn(false);
        when(gameEngine.checkIfPlayerHaveEmptyPits(eq(Player.SECOND),any())).thenReturn(true);

        Game game2 = gameService.makeMove(13, 2, Player.SECOND);

        assertEquals(GameStatus.ENDED, game2.getGameStatus());
        assertEquals(Player.FIRST, game2.getMoveOfPlayer());
    }

    @Test
    void secondPlayerShouldBeWinner() throws WrongPlayerMoveException {
        when(gameRepository.findByGameId(anyLong())).thenReturn(getStubGame(Player.FIRST, createStubBoard(0, 0, 0, 0, 0, 3, 2, 0, 0, 0, 0, 0, 0, 20)));
        when(gameEngine.moveEachStoneIntoKalahForPlayer(any(),any())).thenReturn(getStubGame(Player.FIRST, createStubBoard(0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 22)));
        when(gameEngine.checkIfPlayerHaveEmptyPits(eq(Player.FIRST),any())).thenReturn(true);
        when(gameEngine.checkIfPlayerHaveEmptyPits(eq(Player.SECOND),any())).thenReturn(false);
        when(gameEngine.getGameWinner(any())).thenReturn(Player.SECOND);

        Game game1 = gameService.makeMove(6, 2, Player.FIRST);

        assertEquals(GameStatus.ENDED, game1.getGameStatus());
        assertEquals(Player.SECOND, game1.getMoveOfPlayer());

        when(gameRepository.findByGameId(anyLong())).thenReturn(getStubGame(Player.SECOND, createStubBoard(0, 0, 0, 0, 0, 3, 2, 0, 0, 0, 0, 0, 3, 20)));
        when(gameEngine.moveEachStoneIntoKalahForPlayer(any(),any())).thenReturn(getStubGame(Player.SECOND, createStubBoard(0, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 21)));
        when(gameEngine.checkIfPlayerHaveEmptyPits(eq(Player.FIRST),any())).thenReturn(false);
        when(gameEngine.checkIfPlayerHaveEmptyPits(eq(Player.SECOND),any())).thenReturn(true);

        Game game2 = gameService.makeMove(13, 2, Player.SECOND);

        assertEquals(GameStatus.ENDED, game2.getGameStatus());
        assertEquals(Player.SECOND, game2.getMoveOfPlayer());
    }

    protected Game getStubGame(final Player player, final Map<Integer, Integer> board) {
        final Game game = new Game(board);
        game.setMoveOfPlayer(player);
        return game;
    }

    protected Map<Integer, Integer> createStubBoard(final Integer... values) {
        final Map<Integer, Integer> board = new HashMap<>();
        for (int i = 1; i <= values.length; i++) {
            board.put(i, values[i - 1]);
        }
        return board;
    }
}