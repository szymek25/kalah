package com.szymanski.Kalah.facade.impl;

import com.szymanski.Kalah.constants.Messages;
import com.szymanski.Kalah.dtos.GameStatusDTO;
import com.szymanski.Kalah.dtos.NewGameDTO;
import com.szymanski.Kalah.entities.Game;
import com.szymanski.Kalah.enums.GameStatus;
import com.szymanski.Kalah.enums.Player;
import com.szymanski.Kalah.exceptions.WrongPlayerMoveException;
import com.szymanski.Kalah.services.GameService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class GameFacadeImplTest {


    @Mock
    private Environment environment;

    @Mock
    private GameService gameService;

    @InjectMocks
    private GameFacadeImpl gameFacade;

    @BeforeEach
    void setUp() {
        when(environment.getProperty(eq("local.server.port"))).thenReturn("8080");
        when(environment.getProperty(eq("application.host"))).thenReturn("localhost");
    }

    @Test
    public void createGame() {
        final Game stubGame = new Game(new HashMap<>());
        stubGame.setGameId(1);
        when(gameService.createGame()).thenReturn(stubGame);
        NewGameDTO newGameDTO = gameFacade.createNewGame();

        assertEquals(1, newGameDTO.getGameId());
        assertEquals("http://localhost:8080/api/v1/games/1", newGameDTO.getGameURL());
    }

    @Test
    public void shouldReturnProperGameStatus() throws WrongPlayerMoveException {
        when(gameService.makeMove(anyInt(), anyLong(), any())).thenReturn(createStubGame(Player.FIRST));
        GameStatusDTO gameStatusDTO = gameFacade.makeMove(1, 5, 1);

        assertEquals("http://localhost:8080/api/v1/games/5", gameStatusDTO.getGameURL());
        Map<String, String> status = gameStatusDTO.getStatus();
        assertEquals("2", status.get("1"));
        assertEquals("3", status.get("2"));
        assertEquals("4", status.get("3"));
        assertEquals("5", status.get("4"));
        assertEquals("6", status.get("5"));
        assertEquals("7", status.get("6"));
        assertEquals("0", status.get("7"));
        assertEquals("6", status.get("8"));
        assertEquals("5", status.get("9"));
        assertEquals("4", status.get("10"));
        assertEquals("3", status.get("11"));
        assertEquals("2", status.get("12"));
        assertEquals("1", status.get("13"));
        assertEquals("6", status.get("14"));
        assertEquals(Messages.FIRST_PLAYER_MOVE, gameStatusDTO.getMessage());
        assertEquals(5, gameStatusDTO.getGameId());
    }

    @Test
    public void shouldInformAboutWrongPlayerNumber() {
        GameStatusDTO gameStatusDTO = gameFacade.makeMove(1, 1, 5);
        assertEquals(Messages.WRONG_PLAYER_NUMBER, gameStatusDTO.getMessage());
    }

    @Test
    public void shouldInformAboutWrongPlayerMove() throws WrongPlayerMoveException {
        when(gameService.makeMove(anyInt(), anyLong(), any())).thenThrow(WrongPlayerMoveException.class);
        GameStatusDTO gameStatusDTO = gameFacade.makeMove(1, 5, 1);

        assertEquals(Messages.WRONG_PLAYER_MOVE, gameStatusDTO.getMessage());
    }

    @Test
    public void shouldInformThatGameDoesNotExists() throws WrongPlayerMoveException {
        when(gameService.makeMove(anyInt(), anyLong(), any())).thenThrow(IllegalStateException.class);
        GameStatusDTO gameStatusDTO = gameFacade.makeMove(1, 5, 1);

        assertEquals(Messages.GAME_DOES_NOT_EXIST, gameStatusDTO.getMessage());
    }

    @Test
    public void shouldInformAboutWonGame() throws WrongPlayerMoveException {
        final Game stubGame1 = createStubGame(Player.FIRST);
        stubGame1.setGameStatus(GameStatus.ENDED);
        when(gameService.makeMove(anyInt(), anyLong(), any())).thenReturn(stubGame1);
        GameStatusDTO gameStatusDTO = gameFacade.makeMove(1, 5, 1);

        assertEquals(Messages.FIRST_PLAYER_WON, gameStatusDTO.getMessage());

        final Game stubGame2 = createStubGame(Player.SECOND);
        stubGame2.setGameStatus(GameStatus.ENDED);
        when(gameService.makeMove(anyInt(), anyLong(), any())).thenReturn(stubGame2);
        gameStatusDTO = gameFacade.makeMove(1, 5, 2);

        assertEquals(Messages.SECOND_PLAYER_WON, gameStatusDTO.getMessage());
    }

    @Test
    public void shouldInformAboutDraw() throws WrongPlayerMoveException {
        final Game stubGame1 = new Game(createStubBoard());
        stubGame1.setGameStatus(GameStatus.ENDED);
        stubGame1.setMoveOfPlayer(null);
        when(gameService.makeMove(anyInt(), anyLong(), any())).thenReturn(stubGame1);

        GameStatusDTO gameStatusDTO = gameFacade.makeMove(1, 5, 1);
        assertEquals(Messages.GAME_ENDED_WITH_DRAW, gameStatusDTO.getMessage());

    }

    @Test
    void shouldInformAboutWrongPitNumber() {
        GameStatusDTO gameStatusDTO = gameFacade.makeMove(8, 5, 2);

        assertEquals(Messages.WRONG_PIT_NUMBER, gameStatusDTO.getMessage());
    }


    protected Game createStubGame(final Player player) {
        final Game game = new Game(createStubBoard());
        game.setMoveOfPlayer(player);
        return game;
    }


    protected Map<Integer, Integer> createStubBoard() {
        final Map<Integer, Integer> board = new HashMap<>();
        board.put(1, 2);
        board.put(2, 3);
        board.put(3, 4);
        board.put(4, 5);
        board.put(5, 6);
        board.put(6, 7);
        board.put(7, 0);
        board.put(8, 6);
        board.put(9, 5);
        board.put(10, 4);
        board.put(11, 3);
        board.put(12, 2);
        board.put(13, 1);
        board.put(14, 6);
        return board;
    }
}